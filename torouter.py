#!/usr/bin/env python3

# Script By : Abdennour.py

import sys
import os
import requests
import json
import time
from rich.console import Console
console = Console()

# Tor Router Funcion :
def main():
	os.system("clear")
	console.print(""" 
 _                        _            
| |                      | |           
| |_ ___  _ __ ___  _   _| |_ ___ _ __ 
| __/ _ \| '__/ _ \| | | | __/ _ \ '__|
| || (_) | | | (_) | |_| | ||  __/ |   
 \__\___/|_|  \___/ \__,_|\__\___|_|                                             
	""", style="bold green")
	console.print("🧅#####-Tool By Abdennourpy-#####🧅", style="bold green")
	print("\n")
	console.print(f"[+] Your IP Address ️️👁️: {net_status.ip}", style="bold magenta")	
	# runing the service
	if net_status.status == True:
		console.print("[+] Connection Status : Connected ✅", style="bold green")
		console.print("[*] Do you want to connect again ?", style="bold cyan")
		console.print("*** [1] Yes / [2] No ***", style="bold cyan")
		# choosing to connect again :
		res = input("[*] Enter Number : ")
		if res == "1":
			console.print("[+] Connecting again ...", style="bold magenta")
			anim()
			print("\n")
			os.system(routing_to_tor.cmd)
			net_status()
			console.print(f"[+] Your IP Address : {net_status.ip}", style="bold magenta")
			if net_status.status == True:
				console.print("[+] Connection Status : Connected ✅", style="bold green")
				exit()
			else :
				console.print("[*] Connection Status : Not Connected ❌", style="bold red")
				exit()
		else :
			console.print("[*] Goodbye 👋", style="bold yellow")
			exit()		
	# Starting the service :	
	else :
		console.print("[+] Connection Status : Not Connected ❌", style="bold red")
		console.print("[*] Do you Want to Connect ??", style="bold cyan")
		console.print("*** [1] Yes / [2] No ***", style="bold cyan")
		choice = input("Enter Number : ")
		if choice == "1":
			console.print("[+] Connecting ...", style="bold magenta")
			anim()
			print("\n")
			os.system(routing_to_tor.cmd)
			net_status()
			console.print(f"[+] Your IP Address : {net_status.ip}", style="bold magenta")
			if net_status.status == True:
				console.print("[+] Connection Status : Connected ✅", style="bold green")
				exit()
			else :
				console.print("[*] Connection Status : Not Connected ❌", style="bold red")
				console.print("[*] Goodbye 👋", style="bold yellow")
				exit()
		else :
			console.print("[*] Goodbye 👋", style="bold yellow")
			exit()
			
# Cheking if Tor Service is on :
def net_status():
	net_status.tor_req = requests.get("https://check.torproject.org/api/ip")
	net_status.ip = net_status.tor_req.json()['IP']
	net_status.status = net_status.tor_req.json()['IsTor']

def anim():
	animation = ["[■□□□□□□□□□]","[■■□□□□□□□□]", "[■■■□□□□□□□]", "[■■■■□□□□□□]", "[■■■■■□□□□□]", "[■■■■■■□□□□]", "[■■■■■■■□□□]", "[■■■■■■■■□□]", "[■■■■■■■■■□]", "[■■■■■■■■■■]"]
	for i in range(len(animation)):
		time.sleep(0.1)
		sys.stdout.write("\r" + animation[i % len(animation)])
		sys.stdout.flush()
	
def routing_to_tor():
		routing_to_tor.cmd = "sudo sv restart tor && sudo ./src/tor-router"
		
net_status()
routing_to_tor()
main()
