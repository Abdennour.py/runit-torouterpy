# Runit Torouterpy

Tor Router allow you to use TOR as a transparent proxy and send all your trafic under TOR **INCLUDING DNS REQUESTS**
This version of Tor Router use Python for automation for Runit init system


# Instalation :

**It script require root privileges**
1. Open a terminal and clone the script using the following command:
```
→ git clone https://gitlab.com/Abdennour.py/runit-torouterpy.git
→ cd runit-torouterpy
→ pip install -r requirements.txt
→ chmod +x src/tor-router 
→ chmod +x *
→ ./torouter.py
```

